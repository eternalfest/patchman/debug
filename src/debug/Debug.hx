package debug;

import etwin.ds.FrozenArray;
import hf.entity.Player;
import hf.GameManager;
import hf.Hf;
import hf.mode.GameMode;
import hf.SpecialManager;
import keyboard.Key;
import keyboard.KeyCode;
import patchman.IPatch;
import patchman.module.Env;
import patchman.Ref;

@:build(patchman.Build.di())
class Debug {
  private static inline var WARP_1: KeyCode = KeyCode.NUMPAD_1;
  private static inline var WARP_1_ALT: KeyCode = KeyCode.NUMBER_1;
  private static inline var WARP_2: KeyCode = KeyCode.NUMPAD_2;
  private static inline var WARP_2_ALT: KeyCode = KeyCode.NUMBER_2;
  private static inline var WARP_3: KeyCode = KeyCode.NUMPAD_3;
  private static inline var WARP_3_ALT: KeyCode = KeyCode.NUMBER_3;
  private static inline var WARP_4: KeyCode = KeyCode.NUMPAD_4;
  private static inline var WARP_4_ALT: KeyCode = KeyCode.NUMBER_4;
  private static inline var WARP_5: KeyCode = KeyCode.NUMPAD_5;
  private static inline var WARP_5_ALT: KeyCode = KeyCode.NUMBER_5;
  private static inline var WARP_10: KeyCode = KeyCode.NUMPAD_MULTIPLY;
  private static inline var WARP_10_ALT: KeyCode = KeyCode.NUMBER_0;
  private static inline var WARP_NEXT: KeyCode = KeyCode.NUMPAD_ADD;
  private static inline var WARP_PREV: KeyCode = KeyCode.NUMPAD_SUBTRACT;
  private static inline var WARP_PREV_ALT: KeyCode = KeyCode.NUMBER_9;
  private static inline var KILL_ALL: KeyCode = KeyCode.E;

  public static var FORCE_DEBUG_FLAG(default, never): IPatch =
  Ref.auto(GameManager.main).before(function(hf: Hf, self: GameManager): Void {
    self.fl_debug = true;
  });

  public static var INFINITE_LIVES(default, never): IPatch =
  Ref.auto(Player.killPlayer).before(function(hf: Hf, self: Player): Void {
    if (self.lives <= 0) {
      self.lives = 1;
    }
  });

  public static var EXTRA_CONTROLS(default, never): IPatch =
  Ref.auto(GameMode.getControls).before(function(hf: Hf, self: GameMode): Void {
    var specMan: Null<SpecialManager> = self.getPlayerList()[0].specialMan;

    if (Key.isDown(WARP_1) && self.keyLock != WARP_1.toInt()) {
      specMan.warpZone(1);
      self.keyLock = WARP_1.toInt();
    }
    if (Key.isDown(WARP_1_ALT) && self.keyLock != WARP_1_ALT.toInt()) {
      specMan.warpZone(1);
      self.keyLock = WARP_1_ALT.toInt();
    }
    if (Key.isDown(WARP_2) && self.keyLock != WARP_2.toInt()) {
      specMan.warpZone(2);
      self.keyLock = WARP_2.toInt();
    }
    if (Key.isDown(WARP_2_ALT) && self.keyLock != WARP_2_ALT.toInt()) {
      specMan.warpZone(2);
      self.keyLock = WARP_2_ALT.toInt();
    }
    if (Key.isDown(WARP_3) && self.keyLock != WARP_3.toInt()) {
      specMan.warpZone(3);
      self.keyLock = WARP_3.toInt();
    }
    if (Key.isDown(WARP_3_ALT) && self.keyLock != WARP_3_ALT.toInt()) {
      specMan.warpZone(3);
      self.keyLock = WARP_3_ALT.toInt();
    }
    if (Key.isDown(WARP_4) && self.keyLock != WARP_4.toInt()) {
      specMan.warpZone(4);
      self.keyLock = WARP_4.toInt();
    }
    if (Key.isDown(WARP_4_ALT) && self.keyLock != WARP_4_ALT.toInt()) {
      specMan.warpZone(4);
      self.keyLock = WARP_4_ALT.toInt();
    }
    if (Key.isDown(WARP_5) && self.keyLock != WARP_5.toInt()) {
      specMan.warpZone(5);
      self.keyLock = WARP_5.toInt();
    }
    if (Key.isDown(WARP_5_ALT) && self.keyLock != WARP_5_ALT.toInt()) {
      specMan.warpZone(5);
      self.keyLock = WARP_5_ALT.toInt();
    }
    if (Key.isDown(WARP_10) && self.keyLock != WARP_10.toInt()) {
      specMan.warpZone(10);
      self.keyLock = WARP_10.toInt();
    }
    if (Key.isDown(WARP_10_ALT) && self.keyLock != WARP_10_ALT.toInt()) {
      specMan.warpZone(10);
      self.keyLock = WARP_10_ALT.toInt();
    }
    if (Key.isDown(WARP_NEXT) && self.keyLock != WARP_NEXT.toInt()) {
      specMan.warpZone(1);
      self.keyLock = WARP_NEXT.toInt();
    }
    if (Key.isDown(WARP_PREV) && self.keyLock != WARP_PREV.toInt()) {
      specMan.warpZone(-1);
      self.keyLock = WARP_PREV.toInt();
    }
    if (Key.isDown(WARP_PREV_ALT) && self.keyLock != WARP_PREV_ALT.toInt()) {
      specMan.warpZone(-1);
      self.keyLock = WARP_PREV_ALT.toInt();
    }
    if (Key.isDown(KILL_ALL) && self.keyLock != KILL_ALL.toInt()) {
        for (bad in self.getBadClearList()) {
          bad.forceKill(null);
        }
      self.keyLock = KILL_ALL.toInt();
    }
  });

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  public function new(env: Env): Void {
    var patches: Array<IPatch> = [];
    if (env.isDebug()) {
      patches.push(FORCE_DEBUG_FLAG);
      patches.push(EXTRA_CONTROLS);
      patches.push(INFINITE_LIVES);
    }
    this.patches = FrozenArray.uncheckedFrom(patches);
  }
}
