# 0.10.0 (2021-04-21)

- **[Breaking change]** Update to `@patchman@0.10.3`.
- **[Internal]** Update to Yarn 2.

# 0.9.1 (2020-09-30)

- **[Feature]** Add `E` key to kill all bad in the current level.

# 0.9.0 (2020-09-02)

- **[Breaking change]** Update to `patchman@0.9.0`.

# 0.8.1 (2020-02-29)

- **[Feature]** Add alternative warp keys.

# 0.8.0 (2020-01-08)

- **[Breaking change]** Update to `patchman@0.8.0`.

# 0.7.1 (2020-01-07)

- **[Fix]** Update dependencies.
- **[Fix]** Fix deprecated use of `patchman.Hf`.

# 0.7.0 (2019-12-10)

- **[Breaking change]** Update to `pachman@0.7.1`.

# 0.2.0 (2019-11-02)

- **[Breaking change]** Update to `pachman@0.6.2`.
- **[Breaking change]** Expose patches through dependency injection.

# 0.1.1 (2019-09-19)

- **[Fix]** Fix source code.

# 0.1.0 (2019-09-19)

- **[Feature]** First release.
